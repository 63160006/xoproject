
public class AntiVirusMask {
    private int price;
    private int piece;
    public AntiVirusMask(int price, int piece) {
        this.price = price;
        this.piece = piece;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public int getPiece() {
        return piece;
    }
    public void setPiece(int piece) {
        this.piece = piece;
    }
}
