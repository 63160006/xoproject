import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Lab4_4 {
    public static void main(String[] args) {
        Queue<Integer> Q = new LinkedList<Integer>();
        Scanner kb = new Scanner(System.in);
        
        String menu = kb.next();
        while(!menu.equals("*")){
            if(menu.equals("line")){
                Q.offer(kb.nextInt());
            }else if(menu.equals("give")){
                Integer give = kb.nextInt();

                while(give > 0 && !Q.isEmpty()){
                    if(give >= Q.peek()){
                        give = give - Q.poll();
                    }else{
                        Q.offer(Q.poll()-give);
                        give = 0;
                    }
                }
            }
            menu = kb.next();
        }
        if(Q.isEmpty()){
            System.out.println("empty");
        }else{
            while(!Q.isEmpty()){
                System.out.println(Q.poll());
            }
        }
    }
}
