import java.util.Scanner;
import java.util.Stack;

public class Lab3_4 {
    public static void main(String[] args) {
        Stack<Character> stack = new Stack<Character>();
        Scanner kb = new Scanner(System.in);
        
        String N = kb.next();
        for(int i=0;i<N.length();i++){
            if(N.charAt(i)=='('||N.charAt(i)=='{'||N.charAt(i)=='['){
                stack.push(N.charAt(i));
            }else{
                if(!stack.empty()){
                    if(N.charAt(i)==')'&&stack.peek()=='('){
                        stack.pop();
                    }else if(N.charAt(i)=='}'&&stack.peek()=='{'){
                        stack.pop();
                    }else if(N.charAt(i)==']'&&stack.peek()=='['){
                        stack.pop();
                    }
                }else if(N.charAt(i)==')'||N.charAt(i)=='}'||N.charAt(i)==']'){
                    stack.push(N.charAt(i));
                }
            }
        }if(stack.empty()){
            System.out.println("complete");
        }else{
            System.out.println("incomplete");
        }
    }
}
