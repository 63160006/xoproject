import java.util.ArrayList;
import java.util.Collections;

public class testSortList {
    public static void main(String[] args) {
        
        ArrayList<Student> ar = new ArrayList<Student>();
        ar.add(new Student(111, "bbbb", "london"));
        ar.add(new Student(131, "aaaa", "thai"));
        ar.add(new Student(121, "cccc", "japan"));
        ar.add(new Student(100, "aaaa", "japan"));
        
        Collections.sort(ar, new SortbyId());
        for(Student a:ar){
            System.out.println(a+" ");
        }
    }
}
