import java.util.Scanner;
import java.util.Stack;

public class testBUULogistic {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        int N = kb.nextInt();
        int M = kb.nextInt();
        int Q = kb.nextInt();
        Graph g = new Graph(N);
        for(int i=0; i<M; i++){
            g.addEdge(kb.nextInt(), kb.nextInt());
        }
        for(int i=0; i<Q; i++){
            int A = kb.nextInt();
            int B = kb.nextInt();
            int C = kb.nextInt();
            BreadthFirstPaths bfp = new BreadthFirstPaths(g, B);
            if(bfp.hasPathTo(C)){
                Stack<Integer> thuch = bfp.pathTo(C);
                System.out.println((thuch.size()-1)*A*125);
            }else{
                System.out.println("NO SHIPMENT POSSIBLE");
            }
        }
    }
}
