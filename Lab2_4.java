import java.util.LinkedList;
import java.util.Scanner;

public class Lab2_4 {
    public static void main(String[] args) {
        LinkedList<String> word = new LinkedList<String>();
        Scanner kb = new Scanner(System.in);

        char menu;
        int cursor = 0;
        int N = kb.nextInt();
        for (int i = 0; i < N; i++) {
            menu = kb.next().charAt(0);
            if (menu == 'l') {
                if (cursor != 0) {
                    cursor--;
                }
            } else if (menu == 'r') {
                if (cursor != word.size()) {
                    cursor++;
                }
            } else if (menu == 'i') {
                String w = kb.next();
                word.add(cursor, w);
                cursor++;
            } else if (menu == 'b') {
                if (cursor != 0) {
                    word.remove(cursor - 1);
                    cursor--;
                }
            } else if (menu == 'd') {
                if (cursor != word.size()) {
                    word.remove(cursor);
                }
            }
        }for(String s:word){
            System.out.println(s);
        }
    }
}
