import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class testWorthiness {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        ArrayList<Worthiness> list = new ArrayList<Worthiness>();

        int n = kb.nextInt();

        for(int i=0; i<n; i++){
            list.add(new Worthiness(i+1, kb.nextInt(), kb.nextInt()));
        }
        Collections.sort(list, new SortbyValue());

        int sum=0;
        for(int i=0; i<3; i++){
            sum+=list.get(i).getPrice();
        }
        System.out.println(sum);

        for(int i=0; i<3; i++){
            System.out.println(list.get(i));
        }
    }
}
