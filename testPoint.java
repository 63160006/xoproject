import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class testPoint {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        ArrayList<OrderingPoints> list = new ArrayList<OrderingPoints>();

        int n = kb.nextInt();

        for(int i=0; i<n; i++){
            int x = kb.nextInt();
            int y = kb.nextInt();
            list.add(new OrderingPoints(x, y));
        }
        Collections.sort(list, new SortbyY_des());
        Collections.sort(list, new SortbyX_as());

        for(OrderingPoints p:list){
            System.out.println(p);
        }
    }
}
