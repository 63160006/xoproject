import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class OverSort {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        ArrayList<Integer> Int = new ArrayList<Integer>();
        ArrayList<Double> DB = new ArrayList<Double>();
        ArrayList<Character> Char = new ArrayList<Character>();
        ArrayList<String> Str = new ArrayList<String>();
        int M = kb.nextInt();
        int N;

        if(M==1){
            N = kb.nextInt();
            for(int i=0; i<N; i++){
                Int.add(kb.nextInt());
            }Collections.sort(Int);
            for(int i=0; i<N; i++){
                System.out.print(Int.get(i)+" ");
            }System.out.println();
            Collections.sort(Int, Collections.reverseOrder());
            for(int j=0; j<N; j++){
                System.out.print(Int.get(j)+" ");
            }
        }else if(M==2){
            N = kb.nextInt();
            for(int i=0; i<N; i++){
                DB.add(kb.nextDouble());
            }Collections.sort(DB);
            for(int i=0; i<N; i++){
                System.out.printf("%.0f ",DB.get(i));
            }System.out.println();
            Collections.sort(DB, Collections.reverseOrder());
            for(int j=0; j<N; j++){
                System.out.printf("%.0f ",DB.get(j));
            }
        }else if(M==3){
            N = kb.nextInt();
            for(int i=0; i<N; i++){
                Char.add(kb.next().charAt(0));
            }Collections.sort(Char);
            for(int i=0; i<N; i++){
                System.out.print(Char.get(i)+" ");
            }System.out.println();
            Collections.sort(Char, Collections.reverseOrder());
            for(int j=0; j<N; j++){
                System.out.print(Char.get(j)+" ");
            }
        }else if(M==4){
            N = kb.nextInt();
            for(int i=0; i<N; i++){
                Str.add(kb.next());
            }Collections.sort(Str);
            for(int i=0; i<N; i++){
                System.out.print(Str.get(i)+" ");
            }System.out.println();
            Collections.sort(Str, Collections.reverseOrder());
            for(int j=0; j<N; j++){
                System.out.print(Str.get(j)+" ");
            }
        }
    }
}
