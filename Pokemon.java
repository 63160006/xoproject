public class Pokemon {
    private String name;
    private String type;
    private int cp;

    public Pokemon(String name, String type, int cp){
        this.name = name;
        this.type = type;
        this.cp = cp;

    }
    public String getName(){
        return name;
    }
    public int getCp(){
        return cp;
    }
    public void seetType(String newType){
        type = newType;
    }
    public void setCp(int newCp){
        cp = newCp;
    }
    public String toString(){
        return name+" "+type+" "+cp;
    }
}
