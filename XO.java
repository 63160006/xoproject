import java.util.Scanner;
public class XO {

    static char turn = 'X';
    static String checkWin = "Draw";
    static int endGame = 0;
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String[][] arr = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        System.out.println("Welcome to XO Game");
        
        

        while(endGame == 0){
            if(ConditionCheck(arr)){
                break;
            }
            if(turn=='X'){
                printTable(arr);
                ShowTextTurnX();
                int x,y;
                x=kb.nextInt();
                y=kb.nextInt();
                editXTable(arr, x, y);
            }else{
                printTable(arr);
                ShowTextTurnO();
                int x,y;
                x=kb.nextInt();
                y=kb.nextInt();
                editYTable(arr, x, y);
            }
        }
        WinnerCheck();
    }

    private static void WinnerCheck() {
        if(checkWin.equals("X")){
            System.out.println(">>> X Win <<<");
        }else if(checkWin.equals("O")){
            System.out.println(">>> O Win <<<");
        }else{
            System.out.println(">>> Draw <<<");
        }
    }

    private static void ShowTextTurnO() {
        System.out.println("Turn O");
        System.out.print("Please input row, col: ");
    }

    private static void ShowTextTurnX() {
        System.out.println("Turn X");
        System.out.print("Please input row, col: ");
    }

    private static int drawCheck(String[][] arr) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!arr[i][j].equals("-")) {
                    count++;
                }
            }
        }
        return count;
    }
    private static boolean rightDaiagonalCheck(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            if (arr[0][2].equals(arr[1][1]) && arr[1][1].equals(arr[2][0]) && !arr[0][2].equals("-")) {
                checkWin = arr[0][2];
                printTable(arr);  
                return true;
            }
        }return false;
    }
    private static boolean leftDaiagonalCheck(String[][] arr) {
        if (arr[0][0].equals(arr[1][1]) && arr[1][1].equals(arr[2][2]) && !arr[0][0].equals("-")) {
            checkWin = arr[0][0];
            printTable(arr);   
            return true;    
        }return false;
    }
    private static boolean checkVercital(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            if (arr[0][i].equals(arr[1][i]) && arr[1][i].equals(arr[2][i]) && !arr[0][i].equals("-")) {
                checkWin = arr[0][i];
                printTable(arr);  
                return true;
            }
        }return false;
    }
    private static boolean checkHorizontal(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            if (arr[i][0].equals(arr[i][1]) && arr[i][1].equals(arr[i][2]) && !arr[i][0].equals("-")) {
                checkWin = arr[i][0];
                printTable(arr);
                return true;
            }
        }return false;
    }
    private static void editYTable(String[][] arr, int x, int y) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (x - 1 == i && y - 1 == j && arr[i][j].equals("-")) {
                    arr[i][j] = "O";
                    turn = 'X';
                    break;
                }
            }
        }
    }
    private static void editXTable(String[][] arr, int x, int y) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (x - 1 == i && y - 1 == j && arr[i][j].equals("-")) {
                    arr[i][j] = "X";
                    turn = 'O';
                    break;
                }
            }
        }
    }
    private static void printTable(String[][] arr) {
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                System.out.print(arr[i][j]+" ");
            }
            System.out.println("");
        }
    }

    private static boolean ConditionCheck(String[][] arr) {
        if(checkHorizontal(arr)){
            endGame = 1;
            return true;
        }else{
            if(checkVercital(arr)){
                endGame = 1;
                return true;
            }
            else if(leftDaiagonalCheck(arr)){
                endGame = 1;
                return true;
            }
            else if(rightDaiagonalCheck(arr)){
                endGame = 1;
                return true;
            }
            else if(drawCheck(arr)==9){
                endGame = 1;
                return true;
            }
            
        }
        return false;
    }
}
