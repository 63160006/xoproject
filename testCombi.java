import java.util.ArrayList;
import java.util.Scanner;

public class testCombi {
    static ArrayList<Triple> cached = new ArrayList<Triple>();
    static long combs(int n, int r){
        long v=0;
        if(r==0||r==n){
            v = 1;
        }else{
            boolean found = false;
            for(Triple T: cached){
                if(T.getN()==n && T.getR()==r){
                    v = T.getV();
                    found = true;
                    break;
                }
            }
            if(found==false){
                v = combs(n-1,r)+combs(n-1,r-1);
                cached.add(new Triple(n,r,v));
            }
        }return v;
    }
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt();
        int r = kb.nextInt();
        System.out.println(combs(n, r));
    }
}
