import java.util.ArrayList;
import java.util.Scanner;
public class L1 {
    public static void main(String[] args) {
        Scanner kb = new Scanner (System.in);
        ArrayList<String> num = new ArrayList<String>();
        ArrayList<String> name = new ArrayList<String>();
        ArrayList<Double> grade = new ArrayList<Double>();

        while(true){
            String numm = kb.next();
            if (numm.equals("-1")){
                break;
            }else{
                num.add(numm);
                name.add(kb.next());
                grade.add(kb.nextDouble());
            }
        }
        for (int i = 0;i<num.size();i++){
            for (int j = 0 ;j<num.size();j++){
                if (i!=j){
                    if (num.get(i).equals(num.get(j))){
                        num.remove(j);
                        name.remove(j);
                        grade.remove(j);
                        j--;
                    }
                }
            }
        }
        for (int i = 0;i<num.size();i++){
            System.out.print(num.get(i)+" ");
            System.out.print(name.get(i)+" ");
            System.out.printf("%.2f",grade.get(i));
            System.out.println();
        }
    }
}
