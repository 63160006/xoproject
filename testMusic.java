import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class testMusic {
    public static void main(String[] args) {
        Queue<music> playList = new LinkedList<music>();
        Scanner kb = new Scanner(System.in);

        String menu = kb.next();
        while(!menu.equals("play")){
            if(menu.equals("add")){
                String title = kb.next();
                String singer = kb.next();
                int time = kb.nextInt();

                playList.offer(new music(title,singer,time));
            }else if(menu.equals("del")){
                playList.poll();
            }
            menu = kb.next();
        }
        int playTime = kb.nextInt();
        while(playTime > 0){
            System.out.println(playList.peek());
            playTime = playTime - playList.peek().getTime();
            playList.offer(playList.poll());
        }
    }
}
