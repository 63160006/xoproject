import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class searchV2 {
    public static void main(String[] args) throws IOException {
        BST<String> list = new BST<String>();

        Scanner in = new Scanner(new File("C:\\Users\\tud08\\Downloads\\test03.txt"));
        Scanner kb = new Scanner(System.in);

        while(in.hasNext()){
            //String name = in .nextLine();
            //list.insert(name);
            list.insert(in.nextLine());
        }
        String find = kb.next();
        long start, stop;
        start = System.nanoTime();
        boolean found = list.search(find);
        stop = System.nanoTime();
        
        if(found){
            System.out.println("found");
        }else{
            System.out.println("not found");
        }
        System.out.println("time = " + (stop-start) * 1E-9 + "secs.");
    }
}
