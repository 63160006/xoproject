import java.util.Scanner;

public class testGraph2 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        int M = kb.nextInt();
        int Q = kb.nextInt();
        Graph g = new Graph(1000);
        
        for(int i=0; i<M;i++){
            g.addEdge(kb.nextInt(), kb.nextInt());
        }

        for(int i=0; i<Q; i++){
            int A = kb.nextInt();
            int B = kb.nextInt();

            BreadthFirstPaths bfp = new BreadthFirstPaths(g, A);
            System.out.println(bfp.powerCount(B));
        }
        
    }
}
