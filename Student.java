import java.util.Comparator;

public class Student {
    Integer id;
    String name, address;

    public Student(Integer id, String name, String address){
        this.id = id;
        this.name = name;
        this.address = address;
    }
    public String toString() {
        return this.id + " " + this.name + " " + this.address;
    }
}
class SortbyId implements Comparator<Student>{
    // Used for sorting in ascending order of
    public int compare(Student a, Student b){
        return a.id.compareTo(b.id);
    }
}
class SortbyName implements Comparator<Student>{
    // Used for sorting in ascending order of
    public int compare(Student a, Student b){
        return a.name.compareTo(b.name);
    }
}
