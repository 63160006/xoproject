import java.util.Comparator;

public class Worthiness {
    int no;
    int benefit;
    int price;
    Double value;

    public Worthiness(int no, int benefit, int price){
        this.no = no;
        this.benefit = benefit;
        this.price = price;
        this.value = (double)benefit/price;
    }
    public int getPrice(){
        return price;
    }
    public String toString(){
        return no+" ";
    }
}
class SortbyValue implements Comparator<Worthiness>{
    // Used for sorting in ascending order of
    public int compare(Worthiness a, Worthiness b){
        return b.value.compareTo(a.value);
    }
}
