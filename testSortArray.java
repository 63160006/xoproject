import java.util.Arrays;
import java.util.Collections;

public class testSortArray {
    public static void main(String[] args) {
        Integer[] arr = {13, 7, 6, 45, 21, 9, 2, 100};
        //แบบที่1
        //Arrays.sort(arr);
        Arrays.sort(arr,Collections.reverseOrder());
        for(int i=0; i<arr.length; i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
        //แบบที่2
        for(Integer i: arr){
            System.out.print(i+" ");
        }
        System.out.println();
        //แบบที่3
        System.out.printf("%s",Arrays.toString(arr));
    }
}
