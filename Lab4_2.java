import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Lab4_2 {
    public static void main(String[] args) {
        Queue<Integer> Q = new LinkedList<Integer>();
        Scanner kb = new Scanner(System.in);

        String str1 = kb.next(); //input
        String str2 = kb.next(); //input number
        
        while(!str2.equals("]")){
            Q.offer(Integer.parseInt(str2));
            str2 = kb.next();
        }
        Integer num;
        String menu = kb.next();
        while(!menu.equals("finish")){
            if(menu.equals("enqueue")){
                num = kb.nextInt();
                Q.offer(num);
            }else if(menu.equals("dequeue")){
                Q.poll();
            }
            menu = kb.next();
        }
        boolean found = false;
        int size = Q.size();
        for(int i=1;i<Q.size();i++){
            if(Q.contains(Q.poll())){
                found = true;
                break;
            }
        }
        if(found){
            System.out.println("duplicate");
        }else{
            System.out.println("no duplicate");
        }
    }
}
