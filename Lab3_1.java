import java.util.Scanner;
import java.util.Stack;

public class Lab3_1 {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<String>();
        Stack<Double> num = new Stack<Double>();
        Scanner kb = new Scanner(System.in);

        String fruit = kb.next();
        while(!fruit.equals("*")){
            double N = kb.nextDouble();
            num.push(N);
            stack.push(fruit);
            fruit = kb.next();
        }
        for(int i=0;i<stack.size();i--){
            System.out.println(stack.pop()+" "+num.pop());
        }
    }
}
