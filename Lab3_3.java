import java.util.Scanner;
import java.util.Stack;

public class Lab3_3 {
    public static void main(String[] args) {
        Stack<Integer> num = new Stack<Integer>();
        Scanner kb = new Scanner(System.in);
        
        int count =0;
        int M = Integer.MIN_VALUE;
        int N = kb.nextInt();
        while(N != -1){
            num.push(N);
            N = kb.nextInt();
        }
        while(!num.isEmpty()){
            if(num.peek()>M){
                count++;
                M=num.peek();
            }num.pop();
        }System.out.println(count);
    }
}
