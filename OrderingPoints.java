
import java.util.Comparator;

public class OrderingPoints {
    Integer x;
    Integer y;
    public OrderingPoints(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return x+" "+y;
    }
}
class SortbyX_as implements Comparator<OrderingPoints>{
    // Used for sorting in ascending order of
    public int compare(OrderingPoints a, OrderingPoints b){
        return a.x.compareTo(b.x);
    }
}
class SortbyY_des implements Comparator<OrderingPoints>{
    // Used for sorting in ascending order of
    public int compare(OrderingPoints a, OrderingPoints b){
        return b.y.compareTo(a.y);
    }
}
