import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class SearchV1 {

    public static boolean search(String name, ArrayList<String> list){
        boolean found = false;
        for(String s: list){
            if(s.equals(name)){
                found = true;
                break;
            }
        }
        return found;
    }
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<String>();
        Scanner in = new Scanner(new File("C:\\Users\\tud08\\Downloads\\test03.txt"));
        Scanner kb = new Scanner(System.in);
        
        while(in.hasNext()){
            String name = in.nextLine();
            boolean found = search(name, list);
            if(found==false){
                list.add(name);
            }
        }
        String find = kb.next();
        long start, stop;
        start = System.nanoTime();
        boolean found = search(find, list);
        stop = System.nanoTime();

        if(found){
            System.out.println("found");
        }else{
            System.out.println("not found");
        }
        System.out.println("time = " + (stop-start) * 1E-9 + "secs.");
    }
}                                                                           
