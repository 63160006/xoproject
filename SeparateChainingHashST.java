public class SeparateChainingHashST<Key, Value> {
	private int M = 5;
	private Node[] st = new Node[M];
	private static class Node {
		private Object key;
		private Object val;
		private Node next;

		public Node(Object key2, Object val2, Node node) {
			key = key2;
			val = val2;
			next = node;
		}
	}
	private int hash(Key key) {
		return (key.hashCode() & 0x7FFFFFFF) % M;
	}
	public Value get(Key key) {
		int i = hash(key);	
		for (Node n = st[i]; n != null; n = n.next)
			if (key.equals(n.key))
				return (Value) n.val;
		return null;
	}
	public void put(Key key, Value val) {
		int i = hash(key);

		for (Node n = st[i]; n != null; n = n.next) {
			if (key.equals(n.key)) {
				n.val = val;
				return;
			}
		}
		st[i] = new Node(key, val, st[i]);
	}
	public void printNode() {
		for (int i = 0; i < M; i++) {
			for (Node n = st[i]; n != null; n = n.next) {
				System.out.println(i + " -> " + n.key + " " + n.val);
			}
		}
	}
}