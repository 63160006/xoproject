import java.util.Scanner;
import java.util.HashMap;

public class HashMap3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		Scanner kb = new Scanner(System.in);
		while (true) {
			
			String val = kb.next();
			
			if (val.equals("*")) {
				break;
			}
			
			if (hm.isEmpty()) {
				hm.put(val, 1);
			} else {
				if (hm.containsKey(val)) {
					hm.put(val, hm.get(val) + 1);
				} else {
					hm.put(val, 1);
				}
			}
		}
		System.out.println(hm);

	}

}