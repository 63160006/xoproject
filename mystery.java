public class mystery<T> {
    private T first;
    private T second;

    mystery(T first,T second){
        this.first = first;
        this.second = second;
    }
    public T methodA(){
        return first;
    }
    public T methodB(){
        return second;
    }
    public T methodC(T First){
        return first = First; 
    }
    public T methodD(T Second){
        return second = Second;
    }
    public String toString(){
        return "["+second+", "+first+"]";
    }
}
