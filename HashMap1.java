import java.util.Scanner;

public class HashMap1 {
	
	public static void main(String[] args) {
		LinearProblingHashST<String, Integer> st = new LinearProblingHashST<String, Integer>();
		Scanner kb = new Scanner(System.in);
		while(true) {
			String key = kb.next();
			if(key.equals("*")) break;
			int value = kb.nextInt();
			st.put(key, value);
		}
		String num = kb.next();
	    if(st.get(num) == null) {
	    	System.out.println("not found");
	    }else {
	    	System.out.println(st.get(num));
	    }
	}
}