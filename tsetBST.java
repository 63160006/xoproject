import java.util.Scanner;

public class tsetBST {
    public static void main(String[] args) {
        BST<Integer> tree = new BST<Integer>();
        Scanner kb= new Scanner(System.in);

        String num = kb.next();
        while(!num.equals("*")){
            tree.insert(Integer.parseInt(num));
            num = kb.next();
        }
        tree.printPath();
    }
}
