import javax.swing.JPopupMenu.Separator;

public class testLPH {
    public static void main(String[] args) {
        LinearProblingHashST<String, Integer> ST = new LinearProblingHashST<String, Integer>();

        ST.put("00254", 15);
        ST.put("45340", 20);
        ST.put("55554", 80);
        ST.put("04775", 90);

        String num = "00254";
        if(ST.get(num)==null){
            System.out.println("not found");
        }else{
            System.out.println(ST.get(num));
        }
    }
}
