import java.util.LinkedList;
import java.util.Scanner;

public class Lab2_1 {
    public static void main(String[] args) {
        LinkedList<Pokemon> list = new LinkedList<Pokemon>();

        Scanner kb = new Scanner(System.in);
        String type;
        int cp;
        boolean found;

        String name = kb.next();
        while(!name.equals("*")){

            type = kb.next();
            cp = kb.nextInt();
            found = false;
            for(Pokemon p:list){
                if (p.getName().equals(name)){
                    if (p.getCp() < cp){
                        p.seetType(type);
                        p.setCp(cp);
                    }
                    found = true;
                    break;
                }
            }
            if (found==false){
                list.add(new Pokemon(name, type, cp));
            }
            name = kb.next();
        }
        for(Pokemon p:list){
            System.out.println(p);
        }
    }
}
