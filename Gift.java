public class Gift {
    private String name;
    private int qty;

    public Gift(String name, int qty){
        this.name = name;
        this.qty = qty;
    }
    public String getName(){
        return name;
    }
    public int getQty(){
        return qty;
    }
    public void setQty(int newQty){
        qty = newQty;
    }
    public void upQty(int num){
        qty = qty+num;
    }
    public String toString(){
        return name+" "+qty;
    }
}
