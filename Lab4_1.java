import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Lab4_1 {
    public static void main(String[] args) {
        Queue<String> Q = new LinkedList<String>();
        Scanner kb = new Scanner(System.in);

        String name;
        String menu = kb.next();
        while(!menu.equals("*")){
            if (menu.equals("enqueue")){
                name = kb.next();
                Q.offer(name);
            }else if(menu.equals("dequeue")){
                Q.poll();
            }
            menu = kb.next();
        }
        if(Q.isEmpty()){
            System.out.println("None");
        }else{
            while(!Q.isEmpty()){
                System.out.println(Q.poll());
            }
        }
    }
}
