import java.util.Scanner;
import java.util.Stack;

public class Lab3_2 {
    public static void main(String[] args) {
        Stack<String> web = new Stack<String>();
        Scanner kb = new Scanner(System.in);

        String N = kb.next();
        while (!N.equals("close")) {  
            if (N.equals("open")) {
                System.out.println("my homepage");
            } else if (N.equals("input")) {
                String url = kb.next();
                web.push(url);
                System.out.println(url);
            } else if (N.equals("back")) {
                if(web.isEmpty()){
                    System.out.println("my homepage");
                }else{
                    web.pop();
                    if(web.isEmpty()){
                        System.out.println("my homepage");
                    }else{
                        System.out.println(web.peek());
                    }
                }
            }N = kb.next();
        }  
    }
}
