public class music {
    private String title;
    private String singer;
    private int time;

    public music(String title, String singer,int time){
        this.title = title;
        this.singer = singer;
        this.time = time;
    }
    public int getTime(){
        return time;
    }
    public String toString(){
        return title + " " + singer;
    }
}
