import java.util.Scanner;
import java.util.Stack;

public class mid2 {
    public static void main(String[] args) {

        Stack<Integer> s1=new Stack<Integer>();
        Stack<Integer> s2=new Stack<Integer>();
        Scanner kb=new Scanner(System.in);

        int num =kb.nextInt();
        while(num!=-1){
            if(num%3==0){
                s1.push(num);
            }else{
                s2.push(num);
            }
            num = kb.nextInt();
        }

        String menu = kb.next();
        while(!menu.equals("#")){
            if(menu.equals("A")){
                s1.push(kb.nextInt());
            }else if(menu.equals("B")){
                s2.pop();
            }
            menu=kb.next();
        }
        System.out.println(s1);
    }
}
