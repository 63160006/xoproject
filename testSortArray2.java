import java.util.Arrays;
import java.util.Comparator;

class testSortArray2 {

    public static void main(String[] args) {
        Student[] arr = { new Student(111, "bbbb", "london"),
                new Student(131, "aaaa", "thai"),
                new Student(121, "cccc", "japan"),
                new Student(100, "aaaa", "japan") };

        Arrays.sort(arr, new SortbyId());
        Arrays.sort(arr, new SortbyName());
        for (Student s: arr){
            System.out.println(s);
        }
    }
}

