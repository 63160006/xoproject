import java.util.Scanner;
import java.util.Stack;

public class testGraph {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
    
        int N = kb.nextInt();
        int M = kb.nextInt();
        int Q = kb.nextInt();

        Graph g = new Graph(N);
        for(int i=0; i<M; i++){
            g.addEdge(kb.nextInt(), kb.nextInt());
        }
        for(int i=0; i<Q; i++){
            int origin = kb.nextInt();
            int desinate = kb.nextInt();

            DepthFirstPaths dfp = new  DepthFirstPaths(g, origin);

            if(dfp.hasPathTo(desinate)){
                System.out.println("Yes");
            }else{
                System.out.println("No");
            }
        }
    }
}
