public class Triple {
    private int n;
    private int r;
    private long v;

    Triple(int n,int r,long v){
        this.n = n;
        this.r = r;
        this.v = v;
    }
    public long getN(){
        return n;
    }
    public long getR(){
        return r;
    }
    public long getV(){
        return v;
    }
}
