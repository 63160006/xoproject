import java.util.LinkedList;
import java.util.Scanner;

public class Lab2_3 {

	public static void main(String[] args) {
		LinkedList<Gift> bag1 = new LinkedList<Gift>();
		LinkedList<Gift> bag2 = new LinkedList<Gift>();

		Scanner kb = new Scanner(System.in);
		int N = kb.nextInt();
		int qty, sum, left, left2;
		boolean found1, found2;

		String name = kb.next();
		while (!name.equals("*")) {

			qty = kb.nextInt();

			found1 = false;
			for (Gift g : bag1) {
				if (g.getName().equals(name)) {
					sum = g.getQty() + qty;
					if (sum > N) {
						g.setQty(N);
						left = sum - N;

						found2 = false;
						for (Gift t : bag2) {
							if (t.getName().equals(name)) {
								t.upQty(left);
								found2 = true;
								break;
							}
						}

						if (found2 == false) {
							bag2.add(new Gift(name, left));
						}
					} else {
						g.upQty(qty);
					}
					found1 = true;
					break;
				}
			}
			if (found1 == false) {
				if (qty > N) {
					bag1.add(new Gift(name, N));
					left2 = qty - N;
					bag2.add(new Gift(name, left2));
				} else {
					bag1.add(new Gift(name, qty));
				}
			}

			name = kb.next();
		}

		for (Gift K : bag1) {
			System.out.print(K);
		}
		System.out.println();
		for (Gift K : bag2) {
			System.out.print(K);
		}
	}
}